Welcome to {{ cookiecutter.project_name }}'s documentation!
======================================

.. include:: readme.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   installation
   usage
   modules
   contributing
   {% if cookiecutter.create_author_file == 'y' -%}authors
   {% endif -%}changelog

API Reference
===================

Contents:

.. toctree::
   :maxdepth: 2
   :caption: API reference

   api_reference


Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

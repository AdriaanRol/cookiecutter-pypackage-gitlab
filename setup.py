# !/usr/bin/env python


setup_requirements = [
    "wheel",
    "setuptools",
]

from distutils.core import setup

setup(
    name="cookiecutter-pypackage-gitlab",
    packages=[],
    version="0.1.0",
    description="Cookiecutter template for a Python package",
    author="Audrey Roy Greenfeld",
    license="BSD",
    author_email="",
    url="https://gitlab.com/AdriaanRol/cookiecutter-pypackage-gitlab",
    keywords=["cookiecutter", "template", "package", "gitlab"],
    python_requires=">=3.5",
    setup_requires=setup_requirements,
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "License :: OSI Approved :: BSD License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy",
        "Topic :: Software Development",
    ],
)
